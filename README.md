# TEST MNC
- python 3.8
- create your virtualenv and activate your virtual env
> ``` $ virtualenv .venv ```<br/>
> ``` $ source .venv/bin/activate ```
- update packages
> ``` $ pip install -r requirements.txt ```

## SET .env
- set the mongo_url and secret key from .env
> ``` SECRET_KEY = 't1NP63m4wnBg6nyHYKfmc2TpCOGI4nss' ```<br/>
> ``` MONGO_URL = 'mongodb://localhost/mnc-test' ```

## RUN SERVER
- run the server by running python app.py
> ``` $ python app.py ```

## GET EXAMPLE DATABASE
- get the example for every collection
> ``` user collection = example_data_user.json ```<br/>
> ``` transaction collection = example_data transaction.json ```

## IMPORT POSTMAN
- import existing postman and env for easier testing
> ``` POSTMAN = flask-mongodb.postman_collection.json ```<br/>
> ``` postman env = localtest.postman_environment.json ```
- don't forget to change the url in postman env, the default is http://127.0.0.1:5000 

## REGISTER USER
- path : {URL}/REGISTER
- phone_number has to be unique
- phone number has to be a numeric
- pin has to be a 6 digit

## LOGIN
- path : {URL}/login
- phone_number and has to be a match in db

## TOPUP
- path : {URL}/topup
- has to be authenticated

## PAYMENT
- path : {URL}/payment
- has to be authenticated
- amount has to be greater than the current user balance

## TRANSFER
- path : {URL}/payment
- has to be authenticated
- amount has to be greater than the current user balance
- target user id has to be valid

## TRANSACTION
- path : {URL}/transactions
- has to be authenticated

## UPDATE PROFILE
- path : {URL}/profile
- has to be authenticated
- can only update first_name, last_name and address

## UNITTEST
- run the unittest with pytest:
> ```$ pytest ```

## BACKGROUND PROCESS
- easily check background process from /log/%Y-%M-%D.log
  (automatically created after the first time background process running)

