from flask import Flask
from flask_restful import Api
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager

import environs
from db import initialize_db
from user.routes import user_routes
from transaction.routes import transaction_routes

app = Flask(__name__)

# reading .env file
env = environs.Env()
environs.Env.read_env()


bcrypt = Bcrypt(app)
jwt = JWTManager(app)
api = Api(app)

app.config['SECRET_KEY'] = env('SECRET_KEY')
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = True
app.config['JWT_REFRESH_TOKEN_EXPIRES'] = True
app.config['MONGODB_SETTINGS'] = {
    'host': env('MONGO_URL')
}

# load db
initialize_db(app)
# get every route
user_routes(api)
transaction_routes(api)

if __name__ == '__main__':
    app.run()