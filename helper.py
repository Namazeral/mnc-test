import datetime
import time
import os
import errno
import json

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

def write_log(msg='', level='INFO'):
    mkdir_p(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'log'))
    timestr = time.strftime("%Y-%m-%d")
    f = open("log/{}.log".format(timestr), "a")
    datenow = datetime.datetime.now()
    f.write("{} - {}:{}\n".format(datenow, level, msg))
    f.close()

def create_rsp(result=None, pk=None, msg=None, status=200):
    if type(result) is dict:
        if pk:
            result[pk] = result['_id']
        if result.get('_id'):
            del result['_id']
        if result.get('created_date'):
            result['created_date'] = str(result['created_date'])
        if result.get('updated_date'):
            result['updated_date'] = str(result['updated_date'])
    if status == 200:
        response ={
            'status': 'SUCCESS',
            'result': result
        }
    else:
        response = {'message': msg}
    return response, status








