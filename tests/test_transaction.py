from unittest import TestCase
from mongoengine import connect, disconnect
from flask_bcrypt import check_password_hash
from transaction.models import Transaction

class TestTransaction(TestCase):

    @classmethod
    def setUpClass(cls):
        connect('mongoenginetest', host='mongomock://localhost')

    @classmethod
    def tearDownClass(cls):
        disconnect()

    def test_set_balance(self):
        data = {'id': 'eff8e45a-b8ff-11eb-8456-e71b82849e84', 'amount': 2000, 'type': 'TOPUP'}
        transaction = Transaction(**data)
        transaction.set_balance(5000)
        self.assertEqual(transaction.balance_before, 5000)
        self.assertEqual(transaction.balance_after, 7000)
        data = {'id': 'eff8e45a-b8ff-11eb-8456-e71b82849e84', 'amount': 2000, 'type': 'payment'}
        transaction = Transaction(**data)
        transaction.set_balance(5000)
        self.assertEqual(transaction.balance_before, 5000)
        self.assertEqual(transaction.balance_after, 3000)

    def test_save(self):
        data = {'id': 'eff8e45a-b8ff-11eb-8456-e71b82849e84', 'amount': 2000,
                'balance_before': 2000, 'balance_after': 4000}
        transaction = Transaction(**data)
        transaction.save()
        self.assertIsNotNone(transaction.created_date)
        self.assertIsNotNone(transaction.updated_date)