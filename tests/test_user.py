from unittest import TestCase
from mongoengine import connect, disconnect
from flask_bcrypt import check_password_hash
from user.models import User

class TestUser(TestCase):

    @classmethod
    def setUpClass(cls):
        connect('mongoenginetest', host='mongomock://localhost')

    @classmethod
    def tearDownClass(cls):
        disconnect()

    def test_validate_pin(self):
        data = {
            'first_name':"test-namaz",
            "last_name":"test-eral",
            "phone_number":"01812170112141234",
            "address":"test-rawamangun",
            "pin":"9028312"
        }
        user = User(**data)
        self.assertFalse(user.validate_pin())
        data['pin']="123456"
        user = User(**data)
        self.assertTrue(user.validate_pin())

    def test_set_balance(self):
        data = {'first_name': "test-namaz"}
        user = User(**data)
        user.set_balance()
        self.assertEqual(0, user.balance)
        user.set_balance(1000)
        self.assertEqual(1000, user.balance)

    def test_check_pin(self):
        data = {'pin': "902831"}
        user = User(**data)
        user.hash_pin()
        self.assertTrue(check_password_hash(user.pin, "902831"))

    def test_validate_phone_number(self):
        data = {"phone_number": "01812170112141234a"}
        user = User(**data)
        self.assertFalse(user.validate_phone_number())
        data = {"phone_number": "01812170112141234"}
        user = User(**data)
        self.assertTrue(user.validate_phone_number())

    def test_save(self):
        data = {
            'id': 'eff8e45a-b8ff-11eb-8456-e71b82849e84',
            'first_name': "test-namaz",
            "last_name": "test-eral",
            "phone_number": "01812170112141234",
            "address": "test-rawamangun",
            "pin": "902831"
        }
        user = User(**data)
        user.save()
        self.assertIsNotNone(user.created_date)
        self.assertIsNotNone(user.updated_date)

    def test_update_profile(self):
        data = {
            'id': 'eff8e45a-b8ff-11eb-8456-e71b82849e89',
            'first_name': "test-namaz",
            "last_name": "test-eral",
            "phone_number": "0181217011214123214",
            "address": "test-rawamangun",
            "pin": "902831"
        }
        user = User(**data)
        user.save()
        update_data = {
            "first_name": "Namaz50",
            "last_name": "Eral2",
            "address": "Jl. Diponegoro No. 218"
        }
        result = user.update_profile(update_data)
        del result['updated_date']
        expected_result = {'_id': 'eff8e45a-b8ff-11eb-8456-e71b82849e89', 'first_name': 'Namaz50', 'last_name': 'Eral2',
                           'address': 'Jl. Diponegoro No. 218'}
        self.assertEqual(result, expected_result)







