from db import db
import uuid
import datetime

STATUS = ('SUCCESS', 'PROCESS', 'FAILED')
TRANSACTION_TYPE = ('DEBIT', 'CREDIT')
TYPE = ('TOPUP', 'PAYMENT', 'TRANSFER')


class Transaction(db.Document):
    id = db.UUIDField(primary_key=True, binary=False)
    amount = db.IntField(required=True)
    remarks = db.StringField()
    balance_before = db.IntField(required=True)
    balance_after = db.IntField(required=True)
    status = db.StringField(choices=STATUS, default='PROCESS')
    transaction_type = db.StringField(choices=TRANSACTION_TYPE, default='DEBIT')
    type = db.StringField(choices=TYPE, default='TOPUP')
    user_id = db.UUIDField(binary=False)
    target_user = db.UUIDField(binary=False)
    created_date = db.DateTimeField()
    updated_date = db.DateTimeField(default=datetime.datetime.utcnow())

    meta = {'collection': 'transaction'}

    def set_uuid(self):
        self.id = str(uuid.uuid1())

    def set_balance(self, original_balance):
        self.balance_before = original_balance
        if self.type == 'TOPUP':
            self.balance_after = original_balance + self.amount
        else:
            self.balance_after = original_balance - self.amount

    def save(self, *args, **kwargs):
        if not self.created_date:
            self.created_date = datetime.datetime.utcnow()
        self.updated_date = datetime.datetime.utcnow()
        return super(Transaction, self).save(*args, **kwargs)