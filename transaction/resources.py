from flask import request
from user.models import User
from .models import Transaction
from flask_restful import Resource
from flask_jwt_extended import jwt_required, get_jwt_identity
import threading
from helper import write_log, create_rsp


def transaction_task(user, transaction, target=None):
    try:
        write_log('start transaction {}'.format(transaction.id), 'INFO')
        original_balance = user.balance
        if transaction.type == 'TOPUP':
            updated_balance = user.balance + transaction.amount
        else:
            updated_balance = user.balance - transaction.amount

        if user.balance >= 0:
            transaction.status = 'SUCCESS'
            transaction.balance_before = original_balance
            transaction.balance_after = updated_balance
            user.balance = updated_balance
            transaction.save()
            user.save()
            if transaction.type == 'TRANSFER':
                target.balance = target.balance + transaction.amount
                target.save()
                write_log('transaction {}: successfully transfer {} from user {} to target user {}'
                          .format(transaction.id, transaction.amount, user.id, target.id), 'INFO')
            elif transaction.type == 'PAYMENT':
                write_log('transaction {}: successfully pay {} from user {}'
                          .format(transaction.id, transaction.amount, user.id), 'INFO')
            else:
                write_log('transaction {}: successfully topup {} from user {}'
                          .format(transaction.id, transaction.amount, user.id), 'INFO')
        else:
            write_log('transaction {} FAILED: balance is not enough'.format(transaction.id), 'ERROR')
            transaction = Transaction.objects(id=transaction.id).first()
            transaction.status = 'FAILED'
            transaction.save()
    except Exception as e:
        transaction = Transaction.objects(id=transaction.id).first()
        transaction.status = 'FAILED'
        transaction.save()
        write_log('transaction {} FAILED: {}'.format(transaction.id, str(e)), 'ERROR')


class TopUpApi(Resource):

    @jwt_required(optional=True)
    def post(self):
        user_id = get_jwt_identity()
        if not user_id:
            return create_rsp(msg='Unauthenticated', status=401)
        user = User.objects(id=user_id).first()
        body = request.get_json()
        transaction = Transaction(**body)
        transaction.type = 'TOPUP'
        transaction.user_id = user_id
        transaction.set_balance(user.balance)
        transaction.set_uuid()
        transaction.save()

        threading.Thread(target=transaction_task, args=[user, transaction]).start()

        response = {
            'top_up_id': str(transaction.id),
            'amount': transaction.amount,
            'balance_before': transaction.balance_before,
            'balance_after': transaction.balance_after,
            'created_date': str(transaction.created_date)
        }
        return create_rsp(response)


class PaymentApi(Resource):

    @jwt_required(optional=True)
    def post(self):
        user_id = get_jwt_identity()
        if not user_id:
            return create_rsp(msg='Unauthenticated', status=401)
        user = User.objects(id=user_id).first()
        body = request.get_json()
        transaction = Transaction(**body)
        transaction.type = 'PAYMENT'
        transaction.user_id = user_id
        transaction.set_balance(user.balance)
        if transaction.balance_after < 0:
            return create_rsp(msg='Balance is not enough', status=401)

        transaction.set_uuid()
        transaction.save()

        threading.Thread(target=transaction_task, args=[user, transaction]).start()

        response = {
            'payment_id': str(transaction.id),
            'amount_top_up': transaction.amount,
            'remarks': transaction.remarks,
            'balance_before': transaction.balance_before,
            'balance_after': transaction.balance_after,
            'created_date': str(transaction.created_date)
        }
        return create_rsp(response)


class TransferApi(Resource):

    @jwt_required(optional=True)
    def post(self):
        user_id = get_jwt_identity()
        if not user_id:
            return create_rsp(msg='Unauthenticated', status=401)
        body = request.get_json()
        target = User.objects(id=body.get('target_user')).first()
        if not target or body.get('target_user') == user_id:
            return create_rsp(msg='target is not valid', status=401)

        user = User.objects(id=user_id).first()

        transaction = Transaction(**body)
        transaction.type = 'TRANSFER'
        transaction.user_id = user.id
        transaction.set_balance(user.balance)
        if transaction.balance_after < 0:
            return create_rsp(msg='Balance is not enough', status=401)
        transaction.set_uuid()
        transaction.save()

        threading.Thread(target=transaction_task, args=[user, transaction, target]).start()
        response = {
            'transfer_id': str(transaction.id),
            'amount': transaction.amount,
            'remarks': transaction.remarks,
            'balance_before': transaction.balance_before,
            'balance_after': transaction.balance_after,
            'created_date': str(transaction.created_date)
        }
        return create_rsp(response)


class TransactionApi(Resource):
    @jwt_required(optional=True)
    def get(self):
        user_id = get_jwt_identity()
        if not user_id:
            return create_rsp(msg='Unauthenticated', status=401)
        pipeline = [
            {"$match":{"user_id":user_id}},
            {"$sort": {"created_date": -1}},
            {"$project": {
                "_id": 0, "status": 1, "user_id": 1, "transaction_type": 1, "amount": 1, "remarks": 1,
                "balance_before": 1, "balance_after": 1,
                "created_date": {"$dateToString": {"format": "%Y-%m-%d %H:%M:%S", "date": "$created_date"}},
                "top_up_id": {  # set id to top_up_id if the type is TOPUP
                    "$switch": {
                        "branches": [{"case": {"$eq": ["$type", "TOPUP"]}, "then": '$_id'}],
                        "default": '$None'
                    },
                },
                "transfer_id": {  # set id to transfer_id if the type is transfer
                    "$switch": {
                        "branches": [{"case": {"$eq": ["$type", "TRANSFER"]}, "then": '$id'}],
                        "default": '$None'
                    }
                },
                "payment_id": {  # set id to payment_id if the type is payment
                    "$switch": {
                        "branches": [{"case": {"$eq": ["$type", "PAYMENT"]}, "then": '$id'}],
                        "default": '$None'
                    }
                }
            }
            }
        ]
        transaction = Transaction.objects().aggregate(pipeline)
        return create_rsp(list(transaction))
