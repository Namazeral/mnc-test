from .resources import TopUpApi, TransferApi, PaymentApi, TransactionApi


def transaction_routes(api):
    api.add_resource(TopUpApi, '/topup')
    api.add_resource(TransferApi, '/transfer')
    api.add_resource(PaymentApi, '/pay')
    api.add_resource(TransactionApi, '/transactions')