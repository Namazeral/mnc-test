from db import db
from flask_bcrypt import generate_password_hash, check_password_hash
import uuid
import datetime

class User(db.Document):
    id = db.UUIDField(primary_key=True, binary=False)
    phone_number = db.StringField(required=True, unique=True)
    pin = db.StringField(required=True)
    first_name = db.StringField(required=True)
    last_name = db.StringField(required=True)
    address = db.StringField(required=True)
    balance = db.IntField()
    created_date = db.DateTimeField()
    updated_date = db.DateTimeField(default=datetime.datetime.utcnow())

    meta = {'collection': 'user'}

    def validate_pin(self):
        if not self.pin.isdigit():
            return False
        elif len(self.pin) != 6:
            return False
        return True

    def hash_pin(self):
        self.pin = generate_password_hash(self.pin).decode('utf8')

    def set_balance(self, balance=0):
        self.balance = balance

    def check_pin(self, pin):
        return check_password_hash(self.pin, pin)

    def validate_phone_number(self):
        if not self.phone_number.isnumeric():
            return False
        return True

    def set_uuid(self):
        self.id = str(uuid.uuid1())

    def save(self, *args, **kwargs):
        if not self.created_date:
            self.created_date = datetime.datetime.utcnow()
        self.updated_date = datetime.datetime.utcnow()
        return super(User, self).save(*args, **kwargs)

    def update_profile(self, data):
        update_body = {'updated_date': datetime.datetime.now()}
        if data.get('first_name'):
            update_body.update({'first_name': data.get('first_name')})
        if data.get('last_name'):
            update_body.update({'last_name': data.get('last_name')})
        if data.get('address'):
            update_body.update({'address': data.get('address')})
        if update_body:
            User.objects.get(id=self.id).update(**update_body)
        user = User.objects(id=self.id).only('first_name', 'last_name', 'address',
                                             'updated_date').first().to_mongo().to_dict()
        return user
