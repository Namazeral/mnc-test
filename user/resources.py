from flask import request
from .models import User
from flask_restful import Resource
from flask_jwt_extended import create_access_token, create_refresh_token, jwt_required, get_jwt_identity
import datetime
from helper import create_rsp


class SignupApi(Resource):

    def post(self):
        body = request.get_json()
        validate_user = User.objects(phone_number=body.get('phone_number'))
        if validate_user:
            msg = "Phone Number already registered"
            return create_rsp(msg=msg, status=401)

        user = User(**body)
        if not user.validate_pin():
            msg = "Invalid PIN, pin has to be a 6 digit number"
            return create_rsp(msg=msg, status=401)
        if not user.validate_phone_number():
            msg = "Invalid phone number"
            return create_rsp(msg=msg, status=401)
        user.set_balance()
        user.hash_pin()
        user.set_uuid()
        user.save()
        response = {
            'user_id': str(user.id),
            'first_name': user.first_name,
            'last_name': user.last_name,
            'phone_number': user.phone_number,
            'address': user.address,
            'created_date': str(user.created_date)
        }
        return create_rsp(response)


class LoginApi(Resource):

    def post(self):
        body = request.get_json()
        user = User.objects.get(phone_number=body.get('phone_number'))
        authorized = user.check_pin(body.get('pin'))
        if not authorized:
            msg = "Phone number and pin doesn’t match."
            return create_rsp(msg=msg, status=401)

        expires = datetime.timedelta(days=7)
        access_token = create_access_token(identity=str(user.id), expires_delta=expires)
        refresh_token = create_refresh_token(identity=str(user.id), expires_delta=expires)
        response = {"access_token": access_token, "refresh_token": refresh_token}
        return create_rsp(response)


class UserAPI(Resource):

    @jwt_required(optional=True)
    def put(self):
        user_id = get_jwt_identity()
        if not user_id:
            return create_rsp(msg='Unauthenticated', status=401)
        body = request.get_json()
        user = User.objects.get(id=user_id)
        user = user.update_profile(body)

        return create_rsp(user, 'user_id')