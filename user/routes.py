from .resources import SignupApi, LoginApi, UserAPI

def user_routes(api):
    api.add_resource(SignupApi, '/register')
    api.add_resource(LoginApi, '/login')
    api.add_resource(UserAPI, '/profile')